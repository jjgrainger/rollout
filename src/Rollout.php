<?php

namespace Rollout;

/**
 * Rollout - Simple feature flag API for PHP
 * @version v1
 */
class Rollout
{
    /**
     * An array of features and their conditions
     * @var Array
     */
    protected $features = [];

    /**
     * An array of conditions and their closures
     * @var Array
     */
    protected $conditions = [];

    /**
     * Set a custom condition
     * @param  String  $name    The name of the condition
     * @param  Closure $closure The closure to test the condition
     */
    public function condition($name, Closure $closure)
    {
        // set the condition name and closure
        $this->conditions[$name] = $closure;
    }

    /**
     * Check if the feature is active
     * @param  String  $feature The name of the feature
     * @return boolean          Whether the feature is active or not
     */
    public function isActive($feature)
    {
        // check the feature exists in the configuration
        if (isset($this->features[$feature])) {
            // get the features condition
            $conditon = $this->features[$feature];

            // if the condition exists and is callable
            if (isset($this->conditions[$conditon])) {
                // return the result of the condition check
                return call_user_func($condition);
            }
        }

        // return false by default
        return false;
    }
}
