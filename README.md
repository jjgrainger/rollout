# Rollout v1

> A simple feature flag API for PHP

## Installation

Install with [composer](#).

```
$ composer install jjgrainger/rollout
```

## Getting Started

#### Setup your configuration

```php
$rollout = new Rollout\Rollout;

$rollout->setup([
    'feature1' => 'on',         // always true
    'feature2' => 'off',        // always off
    'feature3' => 'admin|user'  // check against multiple conditions
    'feature4' => 'loggedIn'
]);
```

#### Setup conditions

```php
//...

$rollout->condition('loggedIn', function() {
    if (Auth::user()) {
        return true;
    }

    return false;
});
```

#### Check if feature is active

```php
if ($rollout->isActive('feature4')) {
    //... do stuff
}
```
